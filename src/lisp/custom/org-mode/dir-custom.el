;;; Directory customizations
;;; Venkatesh Choppella


(defvar *top-dir* nil)
(setq *top-dir* "/home/choppell/top/")

(defvar *pvt-dir* nil)
(setq *pvt-dir* (concat *top-dir* "pvt/"))

(defvar *pvt-vxc-dir* nil)
(setq *pvt-vxc-dir* (concat *pvt-dir* "vxc/"))


(defvar *vlead-dir* nil)
(setq *vlead-dir* (concat *top-dir* "vlead/"))

(defvar *rel-work-dirs* nil)
(setq *rel-work-dirs*
  (list
   "acd"
   "adm"
   "etc"
   "lrn"
   "org"
   "prj"
   "prf"
   "pub"
   "res"
   "sys"
   "talks"
   "teach"
   "vlb"
   "vlead"))

(provide 'dir-custom)
