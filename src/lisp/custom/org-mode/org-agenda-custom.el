;;; Org-mode agenda customization
;;; Venkatesh Choppella 27 Mar 2018
;;; updated 9 Sep 2020

(require 'dir-custom)

;;; *top-dir* defined in dir-custom.el to ~/top/

(defvar *work-dirs*
  (mapcar (lambda (d)
			(concat *top-dir* (file-name-as-directory d)))
		  *rel-work-dirs*))
		  
(defun set-agenda-files (d)
  "interactively sets org-agenda-files variable to the list of all org files under given directory"
  (interactive "D")
  (setq org-agenda-files
		(agenda-files-from-dirs (list d))))


		 

(defun agenda-files-from-dirs (ld)
  "returns a list of all org files under the given list of directories"
  (mapcan (lambda (d)
			(directory-files-recursively d ".org$"))
		  ld))

(defun set-agenda (org-dir &rest root-dirs)
  "interactively sets org-agenda-files to all files under root-dirs"
  (interactive)
  (setq *org-dir* org-dir)
  (setq agenda-dir  (concat *org-dir* "agenda/"))
  (setq journal-dir (concat *org-dir* "journal/"))
  (setq refile-file  (concat agenda-dir "refile.org"))
  (setq journal-file (concat journal-dir "journal.org"))
  (setq org-default-notes-file refile-file)
  (setq org-agenda-files (agenda-files-from-dirs root-dirs)))

(defun set-work-agenda ()
  "sets work agenda"
  (interactive)
  (apply 'set-agenda (concat *top-dir* "org/")
			  *work-dirs*))

(defun set-pvt-agenda ()
  "sets pvt agenda"
  (interactive)
  (apply 'set-agenda (concat *pvt-dir* "usr/vxc/org/")
			  (list *pvt-dir*)))

(defun set-vlead-agenda ()
  "sets vlead agenda"  
  (interactive)
  (apply 'set-agenda  (concat *vlead-dir* "org/")
		 (list *vlead-dir*)))


(provide 'org-agenda-custom)

