;;; most of this is copied from norang-custom.el

(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "DEADLINE(l)" "NEXT(n)" "|" "DONE(d)")
              (sequence "WAITING(w@/!)"
						"HOLD(h@/!)"
						"|"
						"CANCELLED(c@/!)"
						"PHONE"
						"MEETING"))))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "sienna" :weight normal)
              ("DEADLINE" :foreground "red" :weight normal)			  
              ("NEXT" :foreground "blue" :weight normal)
              ("DONE" :foreground "forest green" :weight normal)
              ("WAITING" :foreground "orange" :weight normal)
              ("HOLD" :foreground "magenta" :weight normal)
              ("CANCELLED" :foreground "forest green" :weight normal)
              ("MEETING" :foreground "forest green" :weight normal)
              ("PHONE" :foreground "forest green" :weight normal))))


(provide 'org-todo-custom)
