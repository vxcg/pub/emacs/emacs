;;; The routines here 

;;; https://github.com/alpha22jp/atomic-chrome
(require 'atomic-chrome)
(atomic-chrome-start-server)

;;; https://github.com/joostkremers/pandoc-mode
(require 'pandoc-mode)


(defun pandoc-convert (in-format in-mode out-format out-mode)
  "Convert the current buffer in in-format to out-format using
pandoc.  Formats are assumeed identical with file extensions"
  (let* ((input-buf (current-buffer))
		;; output-buf is the result of calling this function
		;; its name is created by prefixing
		;; "pandoc-converted" to the name of the input
		;; buffer and suffixing it with an extension
		;; matching out-format
		 (output-buf-name
		  (concat "pandoc-converted-"
				  (buffer-name input-buf)
				  "." out-format))
		 (output-buf (get-buffer-create output-buf-name))
		 (tmp-file-fqn (make-temp-file "pandoc-converted-" nil
									  (concat "." out-format)))
		 (tmp-file-name (file-name-nondirectory tmp-file-fqn))
		 (tmp-file-dir (file-name-directory tmp-file-fqn)))
	(funcall in-mode)
	(pandoc-mode)
	(pandoc--set 'read in-format)
	(pandoc--set 'write out-format)
	(pandoc--set 'output-dir tmp-file-dir)
	(pandoc--set 'output tmp-file-name)
	(pandoc-run-pandoc)
	;; sleep prevents a potential race condition
	;; between pandoc-run-pandoc above
	;; and insert-file-contents below.
	(sleep-for 1) 
	(switch-to-buffer output-buf)
	(insert-file-contents tmp-file-fqn)
	(funcall out-mode)
	(beginning-of-buffer)
	(current-buffer)))
					
	

(defun pandoc-convert-html-to-org ()
  "converts the current html buffer to org using pandoc"
  (interactive)
  (pandoc-convert "html" 'html-mode "org" 'org-mode))

(defun pandoc-convert-org-to-html ()
  "converts the current org buffer to html using pandoc"
  (interactive)
  (pandoc-convert "org" 'org-mode "html" 'html-mode))


;;; foo -> pandoc-converted-foo.org-> pandoc-converted-pandoc-converted-foo.org.html
(defun pandoc-converted-p (format)
  "Returns non-nil if  the provenance of the current buffer a pandoc
  conversion matching format, nil otherwise"
  (let ((name (buffer-name)))
	(and (string-prefix-p "pandoc-converted-" name)
		 (string-suffix-p format name))))
		
  
(defun pandoc-org-buffer-restore-to-html (&optional target-buffer)
  "Convert the current buffer from org to html.  Then empty the
  target-buffer and copy the resultant html buffer into
  target-buffer.  If target-buffer is non-nil, then it is used as
  target.  If target-buffer is nil and the name of the current
  buffer is itself a result of pandoc
  conversion (pandoc-converted-<bufname>.org), then transform the
  current buffer's name (to <bufname>)and use the buffer of the
  resulting name.  If the target-buffer is nil and the current
  buffer is not a result of pandoc conversion then raise an
  error"
  (interactive "BTarget buffer: \n")
  (let ((org-buffer (current-buffer)))
	(let ((html-buffer (pandoc-convert-org-to-html)))
	  (if (and (not target-buffer)
			   (not (pandoc-converted-p "org")))
		  (error "the target is unspecified 
                  and the current buffer 
                  is not a pandoc-converted org buffer.")
		(switch-to-buffer target-buffer)
		(erase-buffer)
		(insert-buffer html-buffer)
		(kill-buffer org-buffer)
		(kill-buffer html-buffer)
		(switch-to-buffer target-buffer)
		target-buffer))))


;;; saved-buffer (buffer 
;;; (html-mode)
;;; (pandoc-convert-html-to-org)
;;; edit org-mode buffer
;;; copy org-mode buffer into saved buffer



				   












  



	  
	



  

