#+title:  How to build and run the emacs customization kit
#+setupfile:  org-templates/level-0.org
#+author:  Venkatesh Choppella
#+email:  venkatesh.choppell@iiit.ac.in

* Introduction

This manual describes how to clone, build, install and run
Venkatesh's emacs customization kit.  Each of these is driven
by =make=, which is to be run from the directory to which
the customization kit has been cloned. 

* Modular installation
The installation of the customization is modular.
Everything is installed in one directory, which is different
from your default user emacs directory (typically
=~/.emacs.d=).  

The =make -k emacs= is the best way to keep the usage
compartmentalized.  If you wish to delete the customization,
simply =rm -rf= the installation directory.

* Architecture

The modularity extends to the way the =load-path= is
defined.  With the exception of the =elpa= libraries, not
all libraries that installed are part of part of the
=load-path=.  Those to be part of the =load-path= are
explicitly specified in the [[./emacs-config/emacs-config.org][emacs-config.org]] file.

This allows us to selectively add/remove installed
directories to be part of the load-path. 


* Structure of emacs customization directory
Let =ws= be the directory into which the emacs installation
has been cloned.  This directory looks something like this:

#+BEGIN_EXAMPLE
ws/
  build/
    code/            
    docs/
  elisp/ 
  .git/
  .gitignore
  init.sh
  makefile
  src/
#+END_EXAMPLE
Let =top= denote the directory  =ws/build/code/=.  

The structure of the files and directories generated below
=top= are described below:

 - =bash-utils/= :: directory containing the =bash-funs.sh=
                    file, which contains bash functions used
                    by the installer. 

 - =emacs-config/emacs-config.el= :: Emacs package and
      load-path configuration file.

 - =infra/= :: directory where the installation
               infrastructure =.el= files reside.

 - =elpa/= :: directory where Emacs lisp packages will be
              installed by the emacs package manager. 

 - =custom/= :: directory containing subdirectories, each of
                which contains =.el= files. 

 - =git-packages/= :: directory containing git workspaces
      pulled from web repositories.  Each workspace is
      expected to contain =.el= files.


 - =targz-packages/= :: targz packages wgot from the web. 

 - =other/= :: directory containing packages from other
               sources


 - =init.el= ::   Initialization file



#+BEGIN_EXAMPLE
top/
  bash-utils/ ; workspace contian
  custom/
  elpa/
  emacs-config/ 
    emacs-config.el
  git-packages/
  infra/
  init.el
  other/
  targz-packages/
#+END_EXAMPLE



* Installation and usage Steps
** Clone git repository
This clones the customization repository into a local
repository (called =emacs=). 

#+BEGIN_EXAMPLE
git clone  https://gitlab.com/vxcg/prj/emacs/emacs.git
cd emacs
#+END_EXAMPLE

** Customize =emacs-config.org=
   The file =src/org/emacs-config/emacs-config.org= contains
   the customizations.  Feel free to edit the custom
   settings.

** Build =.el= files

This tangles out =.el= files from the =.org= sources. 

#+BEGIN_EXAMPLE
make -k build
#+END_EXAMPLE

** Install packages

This installs packages.  The list of packages is available
from the file [[./package-install.org]].   You may wish to edit
the list in that file.

#+BEGIN_EXAMPLE
make -k install
#+END_EXAMPLE

** Run emacs

This will run =emacs= using the local customization.

#+BEGIN_EXAMPLE
make -k run &
#+END_EXAMPLE

* What you need to already have on your machine

 - ~emacs-25.x~ is installed :: emacs-24 will not work.

 - ~org-version~ is less than 9.x :: ~emacs-25.2.2~ comes
      with ~org-version 8.2.10~ by default.  (This may not
      be true with later versions of ~emacs~, though.).  

