(setq url-proxy-services 
   '((http  . 
;			"http://proxy.iiit.ac.in:8080"
			""
			)
     (https . 
;			"http://proxy.iiit.ac.in:8080"
			""
)))

(defvar export-http-command
	(format "export http_proxy=%s" (cdr (assq 'http url-proxy-services))))
(defvar export-https-command
	(format "export https_proxy=%s" (cdr (assq 'https url-proxy-services))))

(defvar git-packages-leaf-dir "git-packages/")

(defvar git-packages-alist
	  '(
		;; needed for exporting src blocks to html
		("emacs-htmlize" . "https://github.com/hniksic/emacs-htmlize.git")

		;; proof general needed for Coq
		("PG" . "https://github.com/ProofGeneral/PG.git")
		("paren-face" . "https://github.com/tarsius/paren-face.git")
		("prelude" . "https://github.com/bbatsov/prelude.git")

		;; scala
        ("emacs-sbt-mode" . "https://github.com/ensime/emacs-sbt-mode.git")
        ("emacs-scala-mode" . "https://github.com/ensime/emacs-scala-mode.git")

		;; add similar lines here

		;; ox-reveal for doing slides
		("org-reveal" . "https://github.com/yjwen/org-reveal.git")
		;; 
		))

;;; Food for thought for a better design?

;; ("org-9.1.9"     . (:repo-type 'targz :repo "..." :lisp-entries '(contrib/lisp lisp))
;; ("emacs-htmlize" . (:repo-type 'git :repo "..." :lisp-entries '()))


(defvar git-package-entries
  '("emacs-htmlize"
	"PG"
	"paren-face"
	"org-reveal"))

(require 'package)
(add-to-list 'package-archives
			 '("melpa" . "http://melpa.org/packages/"))

(add-to-list 'package-archives
			 '("melpa-stable" . "http://stable.melpa.org/packages/"))

(add-to-list 'package-archives
			 '("org" . "http://orgmode.org/elpa/"))


(defvar elpa-packages-leaf-dir "elpa/")
(defvar elpa-package-list
  '(
		alert
		ammonite-term-repl
		async
		bbdb
		biblio
		biblio-core
		calfw-gcal
		dash
		dash-functional
		deferred
		edit-server
		edit-server-htmlize
		elm-mode
		f
		faceup
		git-commit-mode
		git-rebase-mode
		gntp
		ham-mode
		haskell-mode
		helm
		helm-bibtex
		helm-core
		html-to-markdown
		hydra
		ivy
		js2-mode
		js3-mode
		js-comint
		key-chord
		let-alist
		log4e
		magit
		markdown-mode
		nodejs-repl
		nvm
		ob-ammonite
		openwith
;		org
		org-gcal
		org-journal
        org-ref ; now needs emacs 26
		org-tree-slide
		parsebib
		pdf-tools
		popup
		racket-mode
		request
		request-deferred
		s
		smartparens
		smex
		tablist
		tern
		web-mode
		htmlize
		;;; add more package names below
		use-package
		yasnippet
		auto-complete
		))

(defvar targz-packages-leaf-dir "targz-packages/")

(setq targz-packages-alist
	  '(("org-9.1.9" . "https://orgmode.org/org-9.1.9.tar.gz")
		("auctex-12.3" . "http://ftp.gnu.org/pub/gnu/auctex/auctex-12.3.tar.gz")))

(defvar targz-package-names
  (mapcar (lambda (x) (car x)) targz-packages-alist))

(defvar targz-package-entries
  '(
	"org-9.1.9/lisp"
	"org-9.1.9/contrib/lisp"
	))

(defvar custom-packages-leaf-dir "custom/")
(defvar custom-package-entries
  '(
  "abbrev"
  "dired"
  "faces"
  "hacks"
  "js"
  "keys"
  "misc"
  "ocaml"
  "org-mode"
  "org-mode/contrib/dblock"
  "org-mode/contrib/ox-bibtex"
  "org-mode/contrib/journal"
  "racket"
  "shell"
))

(defvar other-packages-leaf-dir "other/")
(defvar other-package-entries
  '(
  "pathname"
))

(defvar load-path-custom-entries
  (append 
   ;;; custom
   (mapcar (lambda (v) (concat custom-packages-leaf-dir v))
		   custom-package-entries)

   ;;; git-packages
   (mapcar (lambda (v) (concat git-packages-leaf-dir v))
		   git-package-entries)

   ;;; other
   (mapcar (lambda (v) (concat other-packages-leaf-dir v))
		   other-package-entries)

   ;; ;;; targz-packages
   ;; (mapcar (lambda (v) (concat targz-packages-leaf-dir v))
   ;; 		   targz-package-entries)
   ))

(provide 'emacs-config)
