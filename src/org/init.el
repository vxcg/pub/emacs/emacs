(autoload 'helm-bibtex "helm-bibtex" "" t)
(setq bibtex-completion-pdf-field "File") ; use pdf

;;; doesn't seem to work very well with org-ref html export yet.
;;; so park it for now.
;;; (setq bibtex-set-dialect 'biblatex)

;;; Emacs customization
;;; Venkatesh Choppella
;;; <venkatesh.choppella@iiit.ac.in>
;;; gitlab: @vxc

(require 'cl)  ; loaded from emacs distro  
;; (require 'package) ; loaded from emacs distro
(require 'comint)  ; loaded from emacs distro

(message "starting evaluation of init file")

;;; version of emacs running
(message "emacs-version = %s" (version))

(defvar top-dir
  (file-name-as-directory
   (expand-file-name
    (if argv 
      ;; from emacs invoked from makefile
      (elt argv 0)
      ;; when interactively evaluating buffer from build/code/init.el
      default-directory))))

(message "top-dir = %s" top-dir)

(defvar emacs-config-dir (concat top-dir "emacs-config/"))
(setq sav-load-path load-path)
(setq load-path (cons emacs-config-dir load-path))
(message "load-path = %s" load-path)
(require 'emacs-config) ;; this is sitting at top-dir

(require 'package)

(message "package-user-dir = %s" package-user-dir)

;;; set package-user-dir to elpa under 
(setq package-user-dir
	  (file-name-as-directory
;; 	   package-user-dir ; this defaults to ~/.emacs.d/elpa
	   (concat top-dir elpa-packages-leaf-dir)
	   ))

(message "package-user-dir = %s" package-user-dir)

;;; package-directory-list is the list of directories searched for
;;; packages. 
;;; Its value is set by 'package
(message "package-directory-list = %s" package-directory-list)
(package-initialize)										
;(package-install-selected-packages)

;;; pretty print the load-path, one directory per line
(defun pprint-load-path ()
   (mapconcat 'identity load-path  "\n"))
(pprint-load-path)

;; Load Path
;; ---------
;;; (list-of pathname?) -> void?
(defun add-to-load-path (entries)
;  (dolist (entry entries)
;	 (add-to-list 'load-path
;				  (expand-file-name entry top-dir)))
  (setq load-path
		(append (mapcar (lambda (e) (expand-file-name e top-dir)) entries) load-path)))

(message "adding to load-path load-path-custom-entries = %s" load-path-custom-entries)
(add-to-load-path load-path-custom-entries)

(setq frame-width 60) ;; works for bold-24 on my laptop
(setq frame-height 20);; works for bold-24 on my laptop
(setq font-string 
;      "-*-Courier-medium-r-normal-*-18-*-*-*-m-*-iso8859-1"
;      "-*-Courier-medium-r-normal-*-24-*-*-*-m-*-iso8859-1"

;      "-*-Courier-bold-r-normal-*-12-*-*-*-m-*-iso8859-1"
;      "-*-Courier-bold-r-normal-*-18-*-*-*-m-*-iso8859-1" ;      not defined
      "-*-Courier-bold-r-normal-*-24-*-*-*-m-*-iso8859-1"
)

;; font lock
(require 'font-lock) ; loaded from emacs distro
(global-font-lock-mode 1)
;;(font-lock-mode 1)
(load "faces-config")
; lazy lock breaks font-locking in java files
;; (setq font-lock-support-mode 'lazy-lock-mode)
;; frame-width and frame-height come from 
(setq default-frame-alist 
      `((top . 25) (left . 10)
	(width . ,frame-width) (height . ,frame-height)
	(cursor-type . box)
;; colors are set in faces-config.el
	(cursor-color . "red")
;	(font . ,font-string)

))

;;; split windows vertically, 
;;; see responses in the blog
;;; http://galder.zamarreno.com/?p=134

(setq split-width-threshold nil)

;;; Restoring font size when visiting files.

;;; https://groups.google.com/forum/#!topic/gnu.emacs.help/0CVSYlNm9J4
 ;; Restore text-scale after change of major mode.

;; ;;  (setq text-scale-mode-amount 3)
;; (autoload 'text-scale-mode "face-remap")
;; (add-hook 'change-major-mode-hook 
;; 	  (lambda ()
 ;	    (put 'text-scale-mode-amount 'permanent-local t)
;; 	    (text-scale-set 3)
;; 	    ))
;; (add-hook 'after-change-major-mode-hook 'text-scale-mode)

;; (set-face-attribute 'default nil :height 120)
 (add-hook 'after-change-major-mode-hook 
 	  (lambda ()  (text-scale-set 3)))

  ;; mode-line
  (defun set-mode-line ()
    (interactive)
    "sets customized mode-line"
    (setq mode-line-format   
          (list
           "-" 
           'mode-line-mule-info 
           'mode-line-modified 
           '(line-number-mode "L%l--") 
           '(column-number-mode "C%c--") 
           '(-3 . "%p") 
           'mode-line-frame-identification 
           'mode-line-buffer-identification 
           "   " 
           'global-mode-string 
           "   %[(" 
           'mode-name 
           'mode-line-process 
           'minor-mode-alist 
           "%n" 
           ")%]--" 
           '(which-func-mode ("" which-func-format "--")) 
           "-%-")))
(setq column-number-mode t)

;; user@machine frame title
(setq frame-title-format
      (concat  "%b - emacs@" system-name))



;;; For full screen on starting Emacs

(defun toggle-fullscreen ()
  (interactive)
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
			 '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
			 '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0))
  )
(when window-system
  (toggle-fullscreen))


;;; To disable the splash screen
(setq inhibit-splash-screen t)

;;; Display Battery Status
(display-battery-mode t)

;;; Show Column Number
(column-number-mode t)

;;; To show date and time
(setq display-time-day-and-date t
     display-time-12hr-format t)
     (display-time)

;;; changing the text size in the minibuffer
;;; http://stackoverflow.com/questions/7869429/altering-the-font-size-for-the-emacs-minibuffer-separately-from-default-emacs

(add-hook 'minibuffer-setup-hook 'my-minibuffer-setup)
(defun my-minibuffer-setup ()
       (set (make-local-variable 'face-remapping-alist)
          '((default :height 1.5))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(outline-2 ((t (:inherit font-lock-variable-name-face :foreground "steel blue"))))
 '(outline-3 ((t (:inherit font-lock-keyword-face :foreground "dark slate gray")))))

;;; evaluation and debugging
(setq debug-on-error 1)
(setq max-lisp-eval-depth 2000)
(setq max-specpdl-size 6000)
;;; edebug
(setq edebug-trace t)

;; won't beep anymore.
(setq visible-bell t)

;; replace yes or no with y or n
(fset 'yes-or-no-p 'y-or-n-p) 

;;; scroll-lock mode on
(setq scroll-lock-mode t)
(setq scroll-step 1)

;; ediff buffers side-by-side, like it should
(setq ediff-split-window-function 'split-window-horizontally)

;;; Buffers
;;; =======
(defun kill-current-buffer ()
  "Kill the current buffer, without confirmation."
  (interactive)
  (kill-buffer (current-buffer)))

;; kill current buffer without confirmation
(global-set-key "\C-xk" 'kill-current-buffer)

;;; Cursor Motion
;;; ============== 
;;; Normally, `C-n' on the last line of a buffer
;; appends a newline to it.  If the variable
;; `next-line-add-newlines' is `nil', then `C-n' gets an error
;; instead (like `C-p' on the first line).
(setq next-line-add-newlines nil)


;;; always confirm before reverting buffer.
(setq revert-without-query nil)



;;; highlights marked region.
(transient-mark-mode 1)
(setq search-highlight t)
(global-set-key "\C-ce" 'shell-command-on-region)



;;; In Konsole or Terminal on Linux, you highlight what you
;;; want then press Shift+Ctrl+C for copy and Shift+Ctrl+V
;;; for paste.

;; Cut-paste  between emacs and the X-clipboard.
;; see http://blog4gng.blogspot.com/2008_02_01_archive.html
(setq x-select-enable-clipboard t)
(setq interprogram-paste-function
      'x-cut-buffer-or-selection-value)

;;; To set aspell as the default spell-check
;;; ALERT: You might need to install aspell and aspell-en
(setq-default ispell-program-name "aspell")

;;; Set English as the default dictionary
(setq ispell-dictionary "en")

;; British English
(ispell-change-dictionary "british" t)


;;; Turn on flyspell mode
(defun turn-on-flyspell () 
(flyspell-mode t))

(add-hook 'find-file-hooks 'turn-on-flyspell)

;;; Enable ido-mode on startup
;;; (setq ido-enable-flex-matching t)
;;; disable ido
;;; (ido-everywhere 0)
;; (ido-mode t)

;;; file-system-types
(require 'pathname) ;; loaded from other
(setq file-system-types '((unix . unix) (dos . dos)))

;; from sankalp [2012-03-24 Sat]
;; openwith minor mode
;; for opening pdfs in evince, mp3s in some player etc...
(require 'openwith) ;; loaded from elpa
(setq openwith-associations
      '(
        ("\\.pdf\\'"  "evince"  (file))
        ("\\.ps\\'"   "evince"  (file))
        ("\\.doc\\'"  "ooffice" (file))
        ("\\.docx\\'" "ooffice" (file))
        ("\\.xls\\'"  "ooffice" (file))
        ("\\.xlsx\\'" "ooffice" (file))
        ("\\.ppt\\'"  "ooffice" (file))
        ("\\.pptx\\'" "ooffice" (file))
        ;; ("\\.\\(?:jp?g\\|png\\)\\'" "display" (file))
        ))
(openwith-mode t)

;;; Turn on Auto-fill 
(add-hook 'find-file-hooks 'turn-on-auto-fill)

(custom-set-variables
 '(tab-width 4))

(setq tab-always-indent 'complete)

(display-time)
(put 'eval-expression 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(setq resize-mini-windows t)
(setq mode-require-final-newline t)
(setenv "EDITOR" "/usr/bin/emacsclient")

   (fset 'date-trim
		 [?\C-s ?\[ ?\C-b ?\C-c ?f ?\M-f ?\M-f ?\M-f ?\M-d])

;;; Control the fill column when in text mode
(add-hook 'text-mode-hook
	  (function (lambda ()
		      (set-fill-column 
;		       72
;                      70
;		       64
                      60
;                       48  ; ideal for large font
;                       40  ; large font on projector
		       ))))

(defun wc (start end)
  (interactive "r")
  (shell-command-on-region start end "wc"))

;;; Comint
;;; ======
(add-hook 'comint-output-filter-functions
                    'comint-strip-ctrl-m)
     
(add-hook 'comint-output-filter-functions
	  'comint-watch-for-password-prompt)

;;; Turn on parentheses match highlighting
(show-paren-mode 1)


(load "emlib.el")
(setq blink-matching-paren-distance 100000)

;;; custom racket-support.el defined here
(setq racket-racket-program
      "~/apps/racket/racket-7.9/bin/racket"
	  ; "~/apps/racket/racket-6.7/bin/racket"
)

(setq racket-raco-program
       "~/apps/racket/racket-7.9/bin/raco"
      ; "~/apps/racket/racket-6.7/bin/raco"
)


(add-hook 'racket-mode-hook 
	  (function (lambda ()
		      (load "racket-support"))))

(autoload 'haskell-mode "haskell" "Major mode for editing Haskell programs." t)
(add-hook 'haskell-mode-hook
		  (lambda ()
			(interactive-haskell-mode)))

(autoload 'elm-mode "elm" "Major mode for editing Elm programs." t)
(add-to-list 'auto-mode-alist '("\\.elm\\'" . elm-mode))
(add-hook 'elm-mode-hook
		  (lambda ()
			(elm-interactive-mode)))

(add-hook 'scala-mode-hook
		  (lambda ()
			(ammonite-term-repl-minor-mode t)))
;;; (setq ammonite-term-repl-program-args '("-s" "--no-default-predef"))

;;; custom racket-support.el defined here
(setq prolog-program-name "/usr/bin/prolog")
(autoload 'prolog-mode "prolog" "Major mode for editing Prolog programs." t)
(add-to-list 'auto-mode-alist '("\\.plg\\'" . prolog-mode))

(require 'js-comint) ; loaded from elpa
(defun inferior-js-mode-hook-setup ()
  (add-hook 'comint-output-filter-functions 'js-comint-process-output))
(add-hook 'inferior-js-mode-hook 'inferior-js-mode-hook-setup t)
;;; (setq inferior-js-program-command "node --interactive")
(setq inferior-js-program-command "node")
(setq inferior-js-program-arguments '("--interactive"))

;;; javascript
;;; (autoload 'js3-mode "js3" nil t)
;;; (add-to-list 'auto-mode-alist '("\\.js$" . js3-mode))
;;; (add-to-list 'auto-mode-alist '("\\.json$" . js3-mode))

;; (autoload 'tern-mode "tern.el" nil t)
;;; auto-enable tern-mode when running javascript
;; (add-hook 'js-mode-hook (lambda () (tern-mode t)))

;;; coq

;;; see   /home/choppell/venk/emacs/ProofGeneral-4.2/

;; (add-to-load-path '("~/emacs/packages/other/ProofGeneral-4.2/generic"))
;; (load-file "~/emacs/packages/other/ProofGeneral-4.2/generic/proof-site.el")

;; BASH
;; ====
;; Include the following only if you want to run
;; bash as your shell.

;; Setup Emacs to run bash as its primary shell.
(setq shell-file-name "bash")
(setq shell-command-switch "-c")
(setq explicit-shell-file-name shell-file-name)
(setenv "SHELL" shell-file-name)

(defconst shell-prompt-pattern 
  "^\[[A-Za-z0-9]*:[^]]*\]"

  "*Regexp used by Newline command to match subshell prompts.
Anything from beginning of line up to the end of what this pattern matches
is deemed to be a prompt, and is not reexecuted.")

(setq auto-mode-alist
      (cons '("\\.properties$" . shell-script-mode) 
	    auto-mode-alist))

(setq auto-mode-alist
      (cons '("\\.prop$" . shell-script-mode) 
	    auto-mode-alist))

(setq auto-mode-alist
      (cons '("\\.conf$" . shell-script-mode) 
	    auto-mode-alist))

;; shell scripts
(setq auto-mode-alist
      (cons '("\\.sh$" . shell-script-mode) 
	    auto-mode-alist))
(setq auto-mode-alist
      (cons '("\\.bash$" . shell-script-mode) 
	    auto-mode-alist))

;; Makefiles
(setq auto-mode-alist
      (cons '("\\.mak$" . makefile-mode) auto-mode-alist))

(add-hook 'python-mode-hook
	  (function 
	     (lambda ()
	       (setq tab-width 4))))

(load "cd.el")  ;; from emacs/lisp/cd.el
(global-set-key "\C-cd" 'cd-buffer-dir)
(add-hook 'dired-load-hook
  (function 
    (lambda ()
      (setq dired-copy-preserve-time t)
      (setq dired-recursive-copies 'top)
      (setq dired-dwim-target t) ;
					; set dired-do-renames
					; default
					; target to the
					; other window
      (define-key dired-mode-map "b" 'browse-url-of-dired-file)
      (load "dired-x")
      (load "dired-operations")
      (define-key dired-mode-map "z" 'dired-remote-copy)
      (define-key dired-mode-map "r" 'rsync-se-101) 
      (define-key dired-mode-map "T" 'dired-trash-files)
;      (define-key dired-mode-map "\C-ca" dired-acroread-file)
      (define-key dired-mode-map "\C-ca" 'set-agenda-files)
      )))

(load "dired")

(setq debian-emacs-flavor 'emacs24)
;;; setting the debian-emacs-flavor 
;;; is needed for the auctex.el to load correctly
;;; (load "auctex.el" nil t t)
(load "preview-latex.el" nil t t)
(add-hook 'plain-TeX-mode-hook
		  (lambda () 
			(set (make-variable-buffer-local 'TeX-electric-math)
				 (cons "$" "$ "))))
(add-hook 'LaTeX-mode-hook
     	  (lambda () 
			(set (make-variable-buffer-local 'TeX-electric-math)
				 ;; (cons "\\(" "\\)")
				 (cons "$" "$ "))
			(set (make-variable-buffer-local 'LaTeX-electric-left-right-brace) 
				 t)))

;; uncomment if use-package is installed 
;; (use-package graphviz-dot-mode :ensure t)
;; (autoload 'graphviz-dot-mode "dot" "Major mode for editing Dot programs." t)
;; (add-to-list 'auto-mode-alist '("\\.dot\\'" . graphviz-dot-mode))

  (add-to-list 
   'auto-mode-alist
    '("\\.org$" . org-mode))

   (add-hook 'org-mode-hook
             (function (lambda ()
;						 (load "org")
                         (load "org-custom"))))

(setq browse-url-browser-function 
  'browse-url-generic)

(setq browse-url-generic-program
	   "/usr/bin/google-chrome"
 ;; "/usr/bin/firefox"
)

  (require 'edit-server)  ; loaded from elpa
  (when (require 'edit-server nil t)
    (setq edit-server-new-frame nil)
    (edit-server-start))

  (autoload 'edit-server-maybe-dehtmlize-buffer "edit-server-htmlize" "edit-server-htmlize" t)
  (autoload 'edit-server-maybe-htmlize-buffer   "edit-server-htmlize" "edit-server-htmlize" t)
  (add-hook 'edit-server-start-hook 'edit-server-maybe-dehtmlize-buffer)
  (add-hook 'edit-server-done-hook  'edit-server-maybe-htmlize-buffer)

(global-set-key "\C-c\C-g" 'goto-line)
(global-set-key "\C-cl" 'font-lock-mode)
(global-set-key "\C-c\C-b" 'eval-buffer)
(global-set-key "\C-cr" 'repeat-complex-command)
(global-set-key "\C-cc" 'compile)
(global-set-key "\C-cu" 'browse-url-at-point)
(global-set-key "\C-h\C-x\C-f" 'describe-face)
(global-set-key "\C-cw" 'wc)
(global-set-key "\C-cs" 'shell)

(global-set-key ";" 'comment-region)

(define-key global-map [(escape) (control f)]
  'forward-sexp)

(define-key global-map [(escape) (control b)]
'backward-sexp)

(define-key global-map [(escape) (control k)]
'kill-sexp)

(define-key global-map [(control c) (f)]
'strip-forward-sexp)
(define-key global-map [(control c) (b)]
'strip-backward-sexp)

(global-set-key (kbd "C-c [") 'insert-pair)
(global-set-key (kbd "C-c {") 'insert-pair)
(global-set-key (kbd "C-c \"") 'insert-pair)


(define-key global-map [(control x) (r)] 
'shell-command-on-region)

(define-key global-map [(control c) (o)] 'org-iswitchb)
(define-key global-map [(control c) (k)] 'org-capture)

;;; more keys defined here
;;; TODO move above bindings to keys.
;; (load "keys" nil t)

;;; Chris Haynes's Jun 87 customizations
;; (load "emacs" nil t)

;; Goodbye RMAIL
(global-unset-key "\C-xm")

(server-start)

(message "finishing init.el successfully")
(message "load-path = %s" (pprint-load-path))
(message "exiting init.el")
