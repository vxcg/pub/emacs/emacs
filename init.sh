#!/bin/bash
# -*- coding: utf-8 -*-
# Author:  Venkatesh Choppella <venkatesh.choppella@iiit.ac.in>
# Licence: GPL v3.0


# Functions for installing git repositories

echo "publishing-tutorial:  running init.sh with cwd=`pwd`"
online() {
	nc -z 8.8.8.8 53  >/dev/null 2>&1; echo $?
}

git-install() {
	dir=$1
	repo=$2
	if [ -d $dir ]; then
		echo "$dir already installed"
		if [ online ]; then
			echo "online"
			echo "pulling ..."
			git -C $dir pull origin master
		else
			echo "offline"
		fi
	elif [ online ]; then
		echo "cloning ..."
		git clone $repo $dir
	else
		echo "You need to be online to clone the repository." 2>&1
		exit 1
	fi
}

bash_utils=build/code/bash-utils
bash_repo=https://github.com/vlead/bash-utils.git

git-install $bash_utils $bash_repo




